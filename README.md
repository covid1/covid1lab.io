# Covid19 Media Repo

A website for all the media information related to covid19. This is a jekyll website based on Urban template [live demo](https://teal-worm.cloudvent.net/).
This is an effort to create a repo of media related to covid19.


Urban was made by [CloudCannon](https://cloudcannon.com/), the Cloud CMS for Jekyll.

### More details

* More details will be added over the time.
* Help wanted. Feel free to fork this repo and send pull requests.
